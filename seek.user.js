// ==UserScript==
// @name        Seek Listing BS Spotter
// @namespace   tkf144
// @match       https://www.seek.com.au/job/*
// @grant       none
// @version     1.0
// @author      tkf144
// @description 9/15/2022, 1:31:29 PM
// ==/UserScript==

const positivePatterns = [
	/remote(-| )first/i,
	/remote(-| )only/i,
	/fully?(-| )remote/i,
	/totally(-| )remote/i,
	/completely(-| )remote/i,
];

const negativePatterns = [
	/hybrid/i,
	/onsite/i,
	/(role|position) (is |will be )?based in/i,
	/(some|partial|occasional) WFH/i,
	/days? in(-| |the )office/i,
	/in (our |the )?office/i,
];

const styles = `
	.matches-banner {
		font-size: 0.9em;
		font-weight: normal;
		font-family: Roboto, "Helvetica Neue", HelveticaNeue, Helvetica, Arial, sans-serif;
		padding: 20px;
		border-radius: 4px;
		margin: 10px 0;
	}

	#matches-banner-positive {
		color: #254b2d;
		background-color: #bee8c7;
	}

	#matches-banner-negative {
		color: #4b2525;
		background-color: #ffe5e5;
	}

	.matches-banner li {
		margin: 20px;
	}

	.matches-banner *:first-child
	{
		margin-top: 0;
	}

	.matches-banner *:last-child
	{
		margin-bottom: 0;
	}
`;

function run() {
	const listingDetailsElement = document.querySelector("div[data-automation='jobAdDetails']")
	const listingDetailsLines = listingDetailsElement.innerText.split("\n").filter(line => !!line);

	const positiveText = findMatches(listingDetailsLines, positivePatterns);
	const negativeText = findMatches(listingDetailsLines, negativePatterns);

	if(positiveText.length == 0 && negativeText.length == 0) {
		return;
	}

	const newStyles = document.createElement("style");
	newStyles.innerText = styles;
	document.querySelector("head").appendChild(newStyles);

	if(positiveText.length > 0) {
		buildOutputBox("positive", positiveText);
	}

	if(negativeText.length > 0) {
		buildOutputBox("negative", negativeText);
	}
};

function findMatches(detailsLines, patterns) {
	const result = [];
	for(const line of detailsLines) {
		for(const pattern of patterns) {
			const matches = line.match(pattern);
			if(matches) {
				console.log(`Got a match on ${matches[0]} with '${line}'`);
				result.push({ match: matches[0], context: line });
			}
		}
	}
	return result;
}

function buildOutputBox(positiveNegative, text)
{
	const targetEl = document.querySelector("h1[data-automation='job-detail-title']");

	const banner = document.createElement("div");
	banner.id = `matches-banner-${positiveNegative}`;
	banner.classList.add("matches-banner");
	banner.innerHTML = `<p>Listing contains items matched by the ${positiveNegative} patterns:</p>`;

	const list = document.createElement("ul");

	for(const textInstance of text) {
		const listItem = document.createElement("li");
		listItem.innerHTML = `<strong>${textInstance.match}</strong>: <em>"${textInstance.context}</em>"`;
		list.append(listItem);
	}

	banner.append(list);

	targetEl.parentNode.parentNode.insertBefore(banner, targetEl.parentElement);
}

window.addEventListener("load", run);